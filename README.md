# MongoDB

Implementar el uso de mongoDB en la historia de usuario H0020 referidas a "pregúntale al veterinario" de PABIGO para demostrar el manejo de esquemas, datos y relaciones noSQL, tanto con asociaciones embebidas así como asociaciones referenciales.